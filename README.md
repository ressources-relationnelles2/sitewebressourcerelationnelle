# RessourceRelationnelle

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Playwright](https://playwright.dev)

```sh
# Install browsers for the first run
npx playwright install

# When testing on CI, must build the project first
npm run build

# Runs the end-to-end tests
npm run test:e2e
# Runs the tests only on Chromium
npm run test:e2e -- --project=chromium
# Runs the tests of a specific file
npm run test:e2e -- tests/example.spec.ts
# Runs the tests in debug mode
npm run test:e2e -- --debug
```

### Lint with [ESLint](https://eslint.org/)

Ici nous avons Lint pour la qualité de code, lint va parcourir notre code et nous indiquer le code non utilisé.

```sh
npm run lint
```

### Coverage

La partie coverage nous permet de lancer les tests et de vérifier la couverture de test existant.

```sh
npm run coverage
```

## Versioning

Ici la documentation pour la partie versioning

### Majeur

A chaque changement important du site web, par exemple une nouvelle mise en production. La version sera incrémenté de 1. Donc on passerais de la version 1.0.0 à 2.0.0.

Chaque changement de version majeur implique la création d'un tag dans gitlab.

### Mineur

Chaque nouvelle fonctionnalité implique l'incrémentation de la mineur. Par exemple, la fonctionnalité connexion va modifier la version 1.0.0 à la version 1.1.0.

### Fix ou Patch

Chaque bug rencontré et réparé va incrémenter la version fix. Par exemple 1.1.1.

## Documentation MEP

### Dockerfile

Le dockerfile permet de créer une image de notre application et d'un serveur web Nginx afin de le faire tourner.

### Docker Composer

Le dockercompose va créer les 3 environnements :

- Main
- Dev
- Test

Son objectif est de créer les différents service nécessaire, il permet de donner un nom aux containeurs, de définir le port utilisé et les volumes.

### deploy.sh

Il s'agit d'un script pour automatiser le processus de pull et de création de container.
ouvrez un git bash et lancer la commande :
```sh
./deploy.sh
```

### Main

Ici se trouve l'application principale du site qui doit être fonctionnelle 99% du temps.
Le container créé est lié à la branche main.
Fonctionnement :
Le script deploy.sh va venir récupérer le code se trouve sur git pour ensuite monter le container.

Pour exectuer la mise en production : ouvrez un git bash et lancer la commande './deploy.sh'

### Develop

Ici se trouve l'application en cours de développement du site.
Le container créé est lié à la branche develop.
Fonctionnement :
Le script deploy.sh va venir récupérer le code se trouve sur git pour ensuite monter le container.

Pour exectuer la mise en production : ouvrez un git bash et lancer la commande './deploy.sh'

### Test

Ici se trouve l'application en cours de test du site.
Le container créé est lié à la branche test.
Fonctionnement :
Le script deploy.sh va venir récupérer le code se trouve sur git pour ensuite monter le container.

Pour exectuer la mise en production : ouvrez un git bash et lancer la commande './deploy.sh'

## Creation du container initiale

```sh
npm install
```

```sh
npm run build
```

```sh
docker-compose up --build
```

```sh
./deploy.sh
```
