import './assets/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// Importer Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

// Importer votre composant cardsActualite
import CardsActualite from './components/cardsActualite.vue'

// Créer l'application Vue
const app = createApp(App)

// Utiliser le routeur
app.use(router)

// Créer et utiliser Vuetify
const vuetify = createVuetify({
  components,
  directives
})
app.use(vuetify)

// Enregistrer le composant personnalisé
app.component('cards-actualite', CardsActualite)

// Monter l'application
app.mount('#app')
