import { describe, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import Footer from '@/components/footer.vue' // Assurez-vous de remplacer le chemin par le bon

describe('Footer.vue', () => {
  it("rend correctement l'image du logo", () => {
    // Montez le composant Footer
    const wrapper = shallowMount(Footer)

    // Vérifiez si l'image du logo est rendue
    expect(wrapper.find('img.logo').exists()).toBe(true)
  })

  it('rend correctement le texte du pied de page', () => {
    // Montez le composant Footer
    const wrapper = shallowMount(Footer)

    // Vérifiez si le texte du pied de page est rendu correctement
    expect(wrapper.find('p').text()).toBe(
      'Ce site est géré par le Service d’Information du Gouvernement (SIG).'
    )
  })

  it('rend correctement les liens des partenaires', () => {
    // Montez le composant Footer
    const wrapper = shallowMount(Footer)

    // Vérifiez si les liens des partenaires sont rendus correctement
    const partnerLinks = wrapper.findAll('.partenaire a')
    expect(partnerLinks).toHaveLength(3)
    expect(partnerLinks.at(0).attributes('href')).toBe('https://www.legifrance.gouv.fr/')
    expect(partnerLinks.at(1).attributes('href')).toBe('https://www.service-public.fr/')
    expect(partnerLinks.at(2).attributes('href')).toBe('https://www.data.gouv.fr/fr/')
  })

  it('rend correctement le lien de contact', () => {
    // Montez le composant Footer
    const wrapper = shallowMount(Footer)

    // Vérifiez si le lien de contact est rendu correctement
    const contactLink = wrapper.find('a[href="/contact"]')
    expect(contactLink.exists()).toBe(true)
  })
})
