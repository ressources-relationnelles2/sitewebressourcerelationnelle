import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import Footer from '@/components/footer.vue'
import { RouterLinkStub } from '@vue/test-utils'

describe('Footer.vue - Integration Test', () => {
  it('vérifie que toutes les parties du composant Footer sont rendues correctement', () => {
    // Montez le composant Footer avec RouterLink stubbed
    const wrapper = mount(Footer, {
      global: {
        stubs: {
          RouterLink: RouterLinkStub
        }
      }
    })

    // Vérifiez si l'image du logo est rendue
    expect(wrapper.find('img.logo').exists()).toBe(true)

    // Vérifiez si le texte du pied de page est rendu correctement
    expect(wrapper.find('p').text()).toBe(
      'Ce site est géré par le Service d’Information du Gouvernement (SIG).'
    )

    // Vérifiez si les liens des partenaires sont rendus correctement
    const partnerLinks = wrapper.findAll('.partenaire a')
    expect(partnerLinks).toHaveLength(3)
    expect(partnerLinks.at(0).attributes('href')).toBe('https://www.legifrance.gouv.fr/')
    expect(partnerLinks.at(1).attributes('href')).toBe('https://www.service-public.fr/')
    expect(partnerLinks.at(2).attributes('href')).toBe('https://www.data.gouv.fr/fr/')

    // Vérifiez si le lien de contact est rendu correctement
    const contactLink = wrapper.find('a[href="/contact"]')
    expect(contactLink.exists()).toBe(true)
  })
})
