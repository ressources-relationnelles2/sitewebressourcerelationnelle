#!/bin/bash

# Détecter la branche active
BRANCH=$(git rev-parse --abbrev-ref HEAD)

# Pull le code de la branche active
git pull origin $BRANCH

# Construire et lancer le conteneur en fonction de la branche
case "$BRANCH" in
  main)
    echo "Déploiement de la branche main (production)..."
    docker-compose up --build -d prod
    ;;
  test)
    echo "Déploiement de la branche test..."
    docker-compose up --build -d test
    ;;
  develop)
    echo "Déploiement de la branche dev..."
    docker-compose up --build -d dev
    ;;
  *)
    echo "Branche inconnue : $BRANCH. Aucun conteneur ne sera lancé."
    ;;
esac
